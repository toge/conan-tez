from conans import ConanFile, CMake, tools
from conans.model.version import Version
from conans.errors import ConanInvalidConfiguration
import shutil

class TezConan(ConanFile):
    name        = "tez"
    version     = "0.0.0_20200508"
    license     = "zlib"
    author      = "toge.mail@gmail.com"
    url         = ""
    homepage    = "https://github.com/SolraBizna/tez"
    description = "C++14 library for accessing a zip archive embedded within your executable "
    topics      = ("embedded", "zip", "C++14")
    settings    = "os", "compiler", "build_type", "arch"
    generators  = "cmake"
    exports     = "CMakeLists.txt"

    def configure(self):
        if self.settings.compiler.get_safe("cppstd"):
            tools.check_min_cppstd(self, "14")

        minimum_version = {
            "clang": "5",
            "gcc": "5",
            "Visual Studio": "15.0",
        }.get(str(self.settings.compiler))

        if not minimum_version:
            self.output.warn(
                "Unknown compiler {} {}. C++14 compiler is required.".format(self.settings.compiler,
                                                                             self.settings.compiler.version))
        else:
            version = Version(self.settings.compiler.version.value)
            if version < minimum_version:
                raise ConanInvalidConfiguration(
                    "The compiler {} {} does not support C++14".format(self.settings.compiler,
                                                                       self.settings.compiler.version))

    def source(self):
        self.run("git clone https://github.com/SolraBizna/tez/")
        self.run("cd tez && git checkout 1ccbbad0e12b2c93d9a3e2ce0cb15309ddc26b6e")
        shutil.copyfile("CMakeLists.txt", "tez/CMakeLists.txt")

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="tez")
        cmake.build()

    def package(self):
        self.copy("*.hh",    dst="include", src="tez")
        self.copy("*.lib",   dst="lib",     keep_path=False)
        self.copy("*.dll",   dst="bin",     keep_path=False)
        self.copy("*.so",    dst="lib",     keep_path=False)
        self.copy("*.dylib", dst="lib",     keep_path=False)
        self.copy("*.a",     dst="lib",     keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["tez"]
